import numpy as np
import os, glob
 

DATA_DIR = "train"
classes = glob.glob("{}/*".format(DATA_DIR))

train_file = open("train_files.txt","w")
test_file = open("test_files.txt","w")

for i in range(len(classes)):
    files = glob.glob("{}/*".format(classes[i]))
    for fi in files:
        train_file.writelines('{} {}\n'.format(fi, i))
train_file.close()

for i in range(len(classes)):
    files = glob.glob("{}/*".format(classes[i]))
    for fi in files:
        test_file.writelines('{} {}\n'.format(fi, i))
test_file.close()
